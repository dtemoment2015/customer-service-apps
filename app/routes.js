import Login from './components/auth/Login';
import Register from "~/components/auth/Register";
import RestorePassword from "~/components/auth/RestorePassword";
import MainScreen from './components/MainScreen';
import CartPage from './components/CartPage';
import DetalisationPage from './components/DetalisationPage';
import CabinetPage from './components/CabinetPage';
import CategoriesPage from './components/CategoriesPage';
import AboutUs from './components/additional/AboutUs';
import RatingsPage from './components/RatingsPage';
import ScannerPage from './components/ScannerPage';
import FeedbackPage from "~/components/FeedbackPage";
import ProductPage from "~/components/ProductPage";
import ProductsPage from "~/components/ProductsPage";
import FeedbackModal from "~/components/FeedbackModal";
import NoActivatedPage from "~/components/NoActivatedPage";
import NoInternetPage from "~/components/NoInternetPage";
import SalesPage from "~/components/SalesPage";
import SalePage from "~/components/SalePage";
import MyOrdersPage from "~/components/MyOrdersPage";
import OrdersPage from "~/components/OrdersPage";
import OrderPage from "~/components/OrderPage";
import PartnersPage from "~/components/PartnersPage";
import StockPage from "~/components/StockPage";
import LeftoversPage from "~/components/LeftoversPage";
import DocumentsPage from "~/components/DocumentsPage";
import DocumentPage from "~/components/DocumentPage";
import DocumentConsumptionPage from "~/components/DocumentConsumptionPage";
import DocumentComingPage from "~/components/DocumentComingPage";
import ProductsAllPage from "~/components/ProductsAllPage";
import ClientsAllPage from "~/components/ClientsAllPage";

export const routes = {
    '/main-screen': {
        component: MainScreen
    },
    '/ratings-page': {
        component: RatingsPage
    },
    '/scanner-page': {
        component: ScannerPage
    },
    '/detalization-page': {
        component: DetalisationPage
    },
    '/cabinet-page': {
        component: CabinetPage
    },
    '/feedback-modal': {
        component: FeedbackModal
    },
    '/products-page': {
        component: CategoriesPage
    },
    '/products-list': {
        component: ProductsPage
    },
    '/products-card': {
        component: ProductPage
    },
    '/login': {
        component: Login
    },
    '/register': {
        component: Register
    },
    '/about': {
        component: AboutUs
    },
    '/sales': {
        component: SalesPage
    },
    '/sale': {
        component: SalePage
    },
    '/restore': {
        component: RestorePassword
    },
    '/feedback': {
        component: FeedbackPage
    },
    '/cart': {
        component: CartPage
    },
    '/no-internet': {
        component: NoInternetPage
    },
    '/no-activate': {
        component: NoActivatedPage
    },
    '/history-page': {
        component: OrdersPage
    },
    '/history-page-card': {
        component: OrderPage
    },
    '/my-orders': {
        component: MyOrdersPage
    },
    '/partners': {
        component: PartnersPage
    },
    '/stock': {
        component: StockPage
    },
    '/leftovers-page': {
        component: LeftoversPage
    },
    '/documents-page': {
        component: DocumentsPage
    },
    '/document-page': {
        component: DocumentPage
    },
    '/document-consumption-page': {
        component: DocumentConsumptionPage
    },
    '/document-coming-page': {
        component: DocumentComingPage
    },
    '/products_all-page': {
        component: ProductsAllPage
    },
    '/clients_all-page': {
        component: ClientsAllPage
    }
};
