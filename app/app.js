import Vue from 'nativescript-vue';
import VueDevtools from 'nativescript-vue-devtools';
import DateTimePicker from 'nativescript-datetimepicker/vue';
import store from './store';
import Axios from './plugins/axios';
import Filters from './plugins/filters';
import RadSideDrawer from 'nativescript-ui-sidedrawer/vue';
import Navigator from 'nativescript-vue-navigator';
import { routes } from './routes';
import Home from './components/Home';

// import { checkDeviceNews } from './utils/functions';

import { isAndroid } from 'tns-core-modules/platform';
import { TextBase } from 'tns-core-modules/ui/text-base/text-base';

if (isAndroid) {
    TextBase.prototype[require("tns-core-modules/ui/text-base/text-base-common").fontSizeProperty.setNative] = function (value) {
        if (!this.formattedText || (typeof value !== "number")) {
            if (typeof value === "number") {
                this.nativeTextViewProtected.setTextSize(android.util.TypedValue.COMPLEX_UNIT_DIP, value);
            }
            else {
                this.nativeTextViewProtected.setTextSize(android.util.TypedValue.COMPLEX_UNIT_PX, value.nativeSize);
            }
        }
    };
}

// import '@nota/nativescript-accessibility-ext';
// import { Label } from 'tns-core-modules/ui/label';
// import { TextField } from 'tns-core-modules/ui/text-field';
// import { Button } from 'tns-core-modules/ui/button';

// Button.nativeView.setTextSize(android.util.TypedValue.COMPLEX_UNIT_DIP, object.fontSize);
// Label.nativeView.setTextSize(android.util.TypedValue.COMPLEX_UNIT_DIP, object.fontSize);
// TextField.nativeView.setTextSize(android.util.TypedValue.COMPLEX_UNIT_DIP, object.fontSize);

// let elems = [];
// elems.push(Label, TextField, Button);
// elems.forEach(item => {
//     if (item) {
//         item.on(item.loadedEvent, ({ object }) => {
//             if (object.android) {
//                 object.nativeView.setTextSize(android.util.TypedValue.COMPLEX_UNIT_DIP, object.fontSize);
//             }
//         });
//     }
// });

Vue.use(Navigator, { routes });

Vue.registerElement('BarcodeView', () => require('nativescript-barcodeview').BarcodeView);

Vue.registerElement(
    'StatusBar', () => require('nativescript-statusbar').StatusBar
);

Vue.registerElement(
    'IQKeyboardManager', () => require('nativescript-iqkeyboardmanager').IQKeyboardManager
);

Vue.registerElement(
    'PullToRefresh', () => require('@nstudio/nativescript-pulltorefresh').PullToRefresh
);

import RadListView from 'nativescript-ui-listview/vue';

Vue.use(RadListView);
Vue.use(RadSideDrawer);
Vue.use(DateTimePicker);
Vue.use(VueDevtools);
Vue.use(Axios);
Vue.use(Filters);
Vue.prototype.$store = store;

// const application = require('tns-core-modules/application');
// application.on(application.launchEvent, (args) => {
//     let os = args.android ? 2 : 1;
//     let rootView = args.root;
//     // checkDeviceNews(null, os, rootView);
// });

new Vue({
    render: h => h(Home),
    store
}).$start();
