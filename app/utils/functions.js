import { getString, setString } from 'tns-core-modules/application-settings';
import { Toasty, ToastPosition } from 'nativescript-toasty';
import { Color } from 'tns-core-modules/color';

import { getAxiosInstance, getVueInstance } from '../plugins/axios';
import store from '../store/index';
import { prompt, PromptOptions, PromptResult, capitalizationType, inputType } from "tns-core-modules/ui/dialogs";

let tostyStart = true;

import { presentLoader, hideLoader } from '../plugins/activityIndicator';

import { SocketIO } from '@triniwiz/nativescript-socketio';

let socket = new SocketIO('https://customerservice.uz:443');

export function saveAuthToken(token) {
    if (token) {
        setString('Auth', `Bearer ${token}`);
    }
}

export function clearAuthToken() {
    setString('Auth', '');
}

export function getAuthToken() {
    return (getString('Auth')) ? getString('Auth') : '';
}

export function saveChannel(channel) {
    if (channel) {
        setString('Channel', channel);
    }
}

export function getLang() {
    let lang = (getString('lang')) ? getString('lang') : 'ru';
    return lang;
}

export function clearChannel() {
    setString('Channel', '');
}

export function getChannel() {
    return getString('Channel');
}

export function toastyMessage(text, color) {
    if (text && tostyStart) {
        tostyStart = false;
        new Toasty({ text: `   ${text}   ` })
            .setBackgroundColor(new Color(color))
            .setTextColor('#ffffff')
            .setToastPosition(ToastPosition.CENTER)
            .show();
    }
    setTimeout(() => {
        tostyStart = true;
    }, 1000)
}

export function successMessage(text) {
    return toastyMessage(text, '#388e3c');
}

export function errorMessage(text) {
    return toastyMessage(text, '#d32f2f');
}

export async function scanQRActivate(vm) {
    presentLoader();
    new BarcodeScanner().scan({
        cancelLabel: '',
        cancelLabelBackgroundColor: 'white',
        message: '',
        preferFrontCamera: false,
        showFlipCameraButton: false,
        showTorchButton: false,
        torchOn: false,
        resultDisplayDuration: 1500,
        beepOnScan: true,
        openSettingsIfPermissionWasPreviouslyDenied: true,
        closeCallback: () => {
            hideLoader();
        }
    }).then(
        function (result) {
            console.log('2222');
            vm.$axios.post('dealers/activate', {
                activation_code: result.text
            }).then(response => {
                if (response.data.success === true) {
                    userInfo();
                }
                else {
                    alert('Неверный код');
                }
            }).catch((error) => {
                alert('Неверный код');
            }).finally(() => {
                setTimeout(() => {
                    hideLoader();
                }, 700);
            });
        },
        function (errorMessage) {

        }
    );

}

export function savePushNotificationToken(token) {
    setString('NotificationToken', `${token}`);
}

export function getPushNotificationToken() {
    return getString('NotificationToken') ? getString('NotificationToken') : '';
}

export function setUniqueDeviceId(devId) {
    setString('UniqueDeviceID', `${devId}`);
}

export function getUniqueDeviceId() {
    return getString('UniqueDeviceID');
}

//1 ios, 2 android
export function checkDeviceNews(token = null, os = 2, topMost = null) {
    const platform = require('tns-core-modules/platform');
    if (token) {
        savePushNotificationToken(token);
    }
    let axios = getAxiosInstance();
    axios.post(`/device`, {
        device_id: platform.device.uuid,
        token: getPushNotificationToken(),
        language: 'ru',
        timezone: 18000,
        os: os
    }).then(response => {

    });
}

export function userInfo(isFirst = false) {
    if (getAuthToken()) {
        presentLoader();
        let axios = getAxiosInstance();
        axios.get(`/dealers/profile/info`).then(response => {
            if (response.data) {
                store.dispatch('userAction', response.data);
                if (isFirst === true) {
                    store.dispatch('userMenuAction', response.data);
                }
                store.dispatch('setCartCount', response.data.cart_count);
                socketConnect();
            }
        }).catch(response => {
        }).finally(() => {
            hideLoader();
        });
    }
}

export function badge() {
    let axios = getAxiosInstance();
    axios.get(`/dealers/profile/badge`).then(async response => {
        store.commit('CHANGE_BADGES', response.data);
    }).catch(() => {
    }).finally(() => {
    });
}

export function socketConnect() {

    if (store.state.socketInit === false) {

        let Vibrate = require("nativescript-vibrate").Vibrate;
        let vibrator = new Vibrate();
        socket.connect();
        if (getChannel()) {
            socket.on('avtech_database_chat-' + getChannel(), (data => {
                vibrator.vibrate(400);
                store.commit('CHANGE_CHAT', data);
            }));
            socket.on('avtech_database_rating-882220D7A4F4898CCED7E9E8B85EB5DD1D0C45F3CE8EBACB10D4A0F96D5AA639',
            (async data => {
                if (data && store.state.user) {
                    if (data.dealer.id === store.state.user.id) {
                        userInfo();
                        vibrator.vibrate(400);
                    }
                }
            })
        );


            store.commit('CHANGE_SOCKET');
        }
    }

}

export function setToCart(product = null, count = 0) {
    prompt({
        title: this.$store.state.text.alert.set_count,
        message: 'Введите количество:',
        cancelButtonText: this.$store.state.text.alert.cancel,
        defaultText: count ? `${count}` : "1",
        okButtonText: "OK",
        cancelButtonText: "Cancel",
        cancelable: true,
        inputType: inputType.number, // email, number, text, password, or email
        capitalizationType: capitalizationType.sentences // all. none, sentences or words
    }).then((result) => {
        if (result && result.result === true && result.text) {
            if (result.text > 0) {
                presentLoader();
                this.$axios.post(`/v2/dealer/cart/add`, {
                    product_id: product.id,
                    amount: result.text
                }).then(response => {
                    successMessage(this.text.alert.was_added);
                    this.$store.dispatch('setCartCount', response.data.cart_count);
                }).catch(({ data }) => {
                    if (data.errors && data.errors.product_id) {
                        alert(data.errors.product_id[0]);
                    }
                    if (data.errors && data.errors.amount) {
                        alert(data.errors.amount[0]);
                    }
                    hideLoader();
                }).finally(() => {

                });
            }
        }
    }).catch(() => {

    });
}

export function setCodeActivation() {

    this.CHANGE_LOADING(false);
    prompt({
        title: this.$store.state.text.scanner_display.activate_text,
        cancelButtonText: this.$store.state.text.alert.cancel,
        defaultText: "",
        okButtonText: this.$store.state.text.no_activate_display.activate,
        cancelable: true,
        inputType: inputType.text,
        capitalizationType: capitalizationType.sentences
    }).then((result) => {
        if (result && result.result === true && result.text) {
            presentLoader();
            this.$axios
                .post("/dealers/activate/code", {
                    code: result.text,
                })
                .then((res) => {
                    successMessage(this.text.alert.product_activated);
                    setTimeout(() => {
                        this.$navigator.navigate("/detalization-page", {
                            animated: true,
                        });
                    }, 300);
                })
                .catch(({ data }) => {
                    if (data && data.errors && data.errors.code) {
                        alert(data.errors.code[0]);
                        let dataSend = {
                            value: data.errors.code[0],
                            type: 1,
                            client_unix_time: Math.floor(Date.now() / 1000),
                        };
                        this.$axios
                            .post(`/dealers/chat/send`, dataSend)
                            .then((response) => {
                                // this.$navigator.navigate("/feedback", {
                                //     animated: false,
                                // });
                            });
                    }
                })
                .finally(() => {
                    hideLoader();
                });

        }
    }).catch(() => {

    });
}

export function withdraw() {

    this.CHANGE_LOADING(false);
    prompt({
        title: this.$store.state.text.cabinet_display.button.withdraw,
        message: this.$store.state.text.withdraw_sure,
        cancelButtonText: this.$store.state.text.alert.cancel,
        defaultText: "",
        okButtonText: this.$store.state.text.withdraw_button,
        cancelable: true,
        inputType: inputType.number,
        capitalizationType: capitalizationType.sentences
    }).then((result) => {
        if (result && result.result === true && result.text) {
            presentLoader();
            this.$axios
                .post(`/dealers/withdraw`, {
                    sum: result.text,
                    transaction: Date.now()
                })
                .then(response => {
                    userInfo();
                    successMessage(this.text.withdraw_success);
                })
                .catch(({ data }) => {
                    if (data.errors && data.errors.sum) {
                        alert(data.errors.sum[0]);
                    }
                    if (data.errors && data.errors.transaction) {
                        alert(data.errors.transaction[0]);
                    }
                })
                .finally(() => {
                    setTimeout(() => {
                        hideLoader();
                    }, 10);
                });
        }
    }).catch(() => {




    });

}