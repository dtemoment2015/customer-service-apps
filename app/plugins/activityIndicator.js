import {LoadingIndicator} from '@nstudio/nativescript-loading-indicator';

const indicator = new LoadingIndicator();

const options = {
    message: '',
    margin: 0,
    dimBackground: true,
    color: '#666', 
    backgroundColor: '#222',
    userInteractionEnabled: false, 
    hideBezel: false, 
    android: {max: 1}
};

export function presentLoader() {
    indicator.show(options);
}

export function hideLoader() {
    indicator.hide();
}