import axios from 'axios';
import { clearAuthToken, errorMessage, getAuthToken, getLang } from '~/utils/functions';
import config from '~/config';
import { mapMutations, mapState } from 'vuex';
import { hideLoader } from './activityIndicator';
import Login from '~/components/auth/Login';
import NoInternetPage from '~/components/NoInternetPage';

let showNoInternet = false;

const $axios = axios.create({
    baseURL: config.API_URL,
    headers: {
        'Content-Type': 'application/json'
    }
});

let vueInstance = null;

export function getVueInstance() {
    return vueInstance;
}

export function getAxiosInstance() {
    return $axios;
}


$axios.interceptors.request.use((config) => {
    let token = getAuthToken();
    let lang = getLang();
    if (lang) {
        config.headers.common['localization'] = decodeURI(lang);
    }
    if (token) {
        //console.log(token);
        config.headers.common['Authorization'] = decodeURI(token);
    }
    return config;
}, (error) => {
    return Promise.reject(error);
});

$axios.interceptors.response.use((response) => {
    const { status, data, statusText } = response;
    hideLoader();
    if (!status) {
        return connectionError('Ошибка соединения!', this);
    }
    showNoInternet = false;
    return response;
}, ({ response }) => {
    const { status, data, statusText } = response;

    if (parseInt(status) === 401) {
        if (data.errors && data.errors.access) {
            alert(data.errors.access[0]);
        }
        else if (data.message) {
            alert(data.message);
        }
        if (!vueInstance.isLoginPage) {
            clearAuthToken();
            vueInstance.$navigateTo(Login, {
                animated: false,
                clearHistory: true
            });
        }
    }

    if (parseInt(status) === 429) {
        alert('Вы делаете слишком много запросов на сервер. Мы посчитали это как за взлом системы. ' +
            'Если вы не собирались ничего делать, выйдите с приложения и снова зайдите.');
    }

    if (parseInt(status) === 500) {
        alert('Система обновляется. Просим подожать некоторое время. В целях безопасности, мы перенаправим вас на страницу "Вход в аккаунт"');

        if (!vueInstance.isLoginPage) {
            clearAuthToken();
            vueInstance.$navigateTo(Login, {
                animated: false,
                clearHistory: true
            });
        }
        
    }

    vueInstance.setErrors(data);

    hideLoader();

    return Promise.reject(response);
});

function connectionError(error, request) {
    return new Promise((resolve, reject) => {
        if (vueInstance.$navigator.route.component.name !== 'NoInternetPage') {
            vueInstance.$navigateTo(NoInternetPage, {
                animated: false,
                clearHistory: true
            });
            resolve({ noConnection: true });
            showNoInternet = true;
        }
    });

}

export default {
    install(Vue, options) {
        Vue.mixin({
            methods: {
                ...mapMutations(['CHANGE_ERRORS']),

                clearErrors() {
                    this.CHANGE_ERRORS({});
                },

                setErrors(data) {
                    this.clearErrors();
                    if (data) {
                        let { errors } = data;
                        if (errors) {
                            let newErrors = {};
                            for (let i in errors) {
                                Object.assign(newErrors, {
                                    [i]: errors[i][0]
                                });
                            }
                            this.CHANGE_ERRORS(newErrors);
                            return true;
                        }
                    }
                    return false;
                }
            },
            computed: mapState(['isLoginPage']),
            mounted() {
                vueInstance = this;
            }
        });
        Vue.prototype.$axios = $axios;
    }
};
