export default {
    install(Vue) {
        Vue.mixin({
            filters: {
                dateTime(mysql_string) {
                    var t, result = null;
                    if (typeof mysql_string === 'string') {
                        t = mysql_string.split(/[- :]/);
                    }
                    return (t[2] || 0) + '.' + (t[1] || 0) + '.' + (t[0] || 0) + ' ' + (t[3] || 0) + ':' + (t[4] || 0);
                },
                numberWithSpaces(data) {
                    if (data) return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
                }
            }
        })
    }
}
