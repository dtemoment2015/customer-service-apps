import Vue from 'nativescript-vue';
import Vuex from 'vuex';
import { getAxiosInstance, getVueInstance } from '~/plugins/axios';
import { userInfo } from '~/utils/functions';
import { setString } from 'tns-core-modules/application-settings';

Vue.use(Vuex);

let axios = getAxiosInstance();

export default new Vuex.Store({
    state: {
        loggedIn: false,
        errors: {},
        user: null,
        isLoginPage: false,
        mainScreen: true,
        badges: null,
        loadingGlobal: false,
        event_user: null,
        chat: null,
        text: null,
        language: 'ru',
        menu: [],
        tab: [],
        main_screen: [],
        socketInit: false,
        cart_count: 0
    },

    mutations: {
        CHANGE_MENU_ITEMS(state, payload) {
            state.menu = payload
        },
        CHANGE_SCREEN_ITEMS(state, payload) {
            state.main_screen = payload
        },
        CHANGE_TAB_ITEMS(state, payload) {
            state.tab = payload
        },
        CHANGE_STATE(state, payload) {
            state.loggedIn = payload;
        },
        CHANGE_ERRORS(state, payload) {
            state.errors = payload;
        },
        CHANGE_LOADING(state, payload) {
            state.loadingGlobal = payload;
        },
        IS_LOGIN(state, payload) {
            state.isLoginPage = !!(payload);
        },
        CHANGE_USER(state, payload) {
            state.user = payload;
        },
        CHANGE_EVENT_USER(state, payload) {
            state.event_user = payload;
        },
        CHANGE_CHAT(state, payload) {
            state.chat = payload;
        },
        CHANGE_MAIN_SCREEN(state, payload) {
            state.mainScreen = payload;
        },
        CHANGE_BADGES(state, payload) {
            state.badges = payload;
        },
        CHANGE_TEXT(state, payload) {
            state.text = payload;
        },
        CHANGE_LANGUAGE(state, payload) {
            state.language = payload;
        },
        CHANGE_SOCKET(state) {
            state.socketInit = true;
        },
        SET_CART_COUNT(state, payload) {
            state.cart_count = payload;
        }
    },
    actions: {
        setCartCount({ commit }, cart_count) {
            commit('SET_CART_COUNT', cart_count);
        },
        textAction({ commit }) {
            // presentLoader();
            axios.get(`/system-text`).then(response => {
                commit('CHANGE_TEXT', response.data);
            }).catch(response => {
            }).finally(() => {
                userInfo(true);
            });
        },

        async languageAction({ commit, dispatch }, lang) {
            setString('lang', lang);
            commit('CHANGE_LANGUAGE', lang);
            await dispatch('textAction');
        },

        userAction({ commit }, user) {
            if (user) {
                commit('CHANGE_USER', user);
            }
        },
        userMenuAction({ commit }, user) {
            if (user) {
                commit('CHANGE_MENU_ITEMS', user.role.nav.menu);
                commit('CHANGE_TAB_ITEMS', user.role.nav.tab);
                commit('CHANGE_SCREEN_ITEMS', user.role.nav.main_screen);
            }
        }

    },
    getters: {
        loggedIn: state => state.loggedIn,
        isLoginPage: state => state.isLoginPage,
        errors: state => state.errors,
        user: state => state.user,
        event_user: state => state.event_user,
        mainScreen: state => state.mainScreen,
        badges: state => state.badges,
        chat: state => state.chat,
        text: state => state.text,
        language: state => state.language
    }
});
